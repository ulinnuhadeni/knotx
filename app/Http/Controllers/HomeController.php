<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function homepage()
    {
        return view('pages.main');
    }

    public function listing()
    {
        return view('pages.listing.main');
    }
}
