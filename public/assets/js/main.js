/*---------------------------------------------
Template name:  Listhub
Item Description: Listhub - Directory & Listing HTML Template
Version:        1.0
Author:         TechyDevs
Author Email:   contact@tecydevs.com

[Table of Content]

01: Preloader
02: side-widget-menu
02: Mobile Menu Open Control
03: Mobile Menu Close Control
04: Side user panel menu Open Control
05: Back to Top Button and Navbar Scrolling Effects
06: back to top button click control
07: most-visited-wrap
08: Client logo slider
09: client-testimonial
10: gallery-carousel
11: Fancybox js
12: Daterangepicker
13: Quantity number increment control
14: Quantity number decrement control
15: Tooltip
17: Counter up
----------------------------------------------*/

(function ($) {
    "use strict";

    var $window = $(window);

    $window.on('load', function () {

        var $document = $(document);
        var $dom = $('html, body');
        var preloader = $('.loader-container');
        var dropdownMenu = $('.main-menu-content .dropdown-menu-item');
        var userChosenSelect = $('.user-chosen-select');
        var isMenuOpen = false;
        var numberCounter = $('.counter');
        var userTextEditor = $('.user-text-editor');
        var backToTopBtn = $('#back-to-top');
        var cardCarousel = $('.card-carousel');
        var cardCarouselTwo = $('.card-carousel-2');
        var cardCarouselThree = $('.card-carousel-3');
        var clientLogoCarousel = $('.client-logo');
        var testimonialCarousel = $('.testimonial-carousel');
        var galleryCarousel = $('.gallery-carousel');
        var dateDropperPicker = $('.date-dropper-input');
        var datePicker = $('.date-input');
        var fullScreenSlider = $('.full-screen-slider');
        var onlineUserSlider = $('.online-user-slider');
        var emojiPicker = $('.emoji-picker');
        var scrollLink = $('#sticky-content-nav .scroll-link');
        var singleSlider = $('.single-slider');
        var jsTilt = $('.js-tilt');
        var jsTiltTwo = $('.js-tilt-2');
        var lazyLoading = $('.lazy');
        var selectCategory = $('#select-category');

        /* ======= Preloader ======= */
        preloader.delay('500').fadeOut(2000);

        /*====  sidebar menu =====*/
        $document.on('click', '#sidebarToggleTop', function () {
            $('.dashboard-sidebar').addClass('sidebar-is-active');
        });

        /*====  sidebar menu =====*/
        $document.on('click', '#sidebar-close', function () {
            $('.dashboard-sidebar').removeClass('sidebar-is-active');
        });

        /*=========== Mobile Menu Open Control ============*/
        $document.on('click','.menu-toggle', function () {
            $(this).toggleClass('active');
            $('.main-menu-content').slideToggle(200);
        });

        /*=========== Dropdown menu ============*/
        dropdownMenu.parent('li').children('a').append(function() {
            return '<span class="drop-menu-toggle"><i class="la la-plus"></i></span>';
        });

        /*=========== Dropdown menu ============*/
        $document.on('click', '.drop-menu-toggle', function() {
            var Self = $(this);
            Self.toggleClass('active');
            Self.parent().parent().children('.dropdown-menu-item').toggle();
            return false;
        });

        /*=========== When window will resize then this action will work ============*/
        $window.on('resize', function () {
            if ($window.width() > 1200) {
                $('.main-menu-content').show();
                $('.dropdown-menu-item').show();
            }else {
                if (isMenuOpen) {
                    $('.main-menu-content').show();
                    $('.dropdown-menu-item').show();
                }else {
                    $('.main-menu-content').hide();
                    $('.dropdown-menu-item').hide();
                }
            }
        });

        /*=========== Side user panel menu Open Control ============*/
        $document.on('click','.header-search', function () {
            $(this).toggleClass('active');
        });

        $document.on("click", function(event){
            var $trigger = $(".header-search");
            if($trigger !== event.target && !$trigger.has(event.target).length){
                $(".header-search").removeClass("active");
            }
        });

        /*===== Back to Top Button and Navbar Scrolling Effects ======*/
        $window.on('scroll', function() {
            //header fixed animation and control
            if($window.scrollTop() > 10) {
                $('.header-menu-wrapper').addClass('header-fixed');
                $('.header-top-bar').hide(200);
            }else{
                $('.header-menu-wrapper').removeClass('header-fixed');
                $('.header-top-bar').show(200);
            }

            //back to top button control
            if ($window.scrollTop() > 300) {
                $(backToTopBtn).addClass('btn-active');
            } else {
                $(backToTopBtn).removeClass('btn-active');
            }

            //page scroll position
            findPosition();

        });

        /*===== back to top button click control ======*/
        $document.on("click", '#back-to-top', function() {
            $($dom).animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        /*==== card-carousel =====*/
        if ($(cardCarousel).length) {
            $(cardCarousel).owlCarousel({
                loop: true,
                items: 3,
                nav: true,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                center: true,
                margin: 30,
                navText: ["<i class=\"la la-arrow-left\"></i>", "<i class=\"la la-arrow-right\"></i>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1
                    },
                    // breakpoint from 992 up
                    992 : {
                        items: 3
                    }
                }
            });
        }

        /*==== card-carousel-2 =====*/
        if ($(cardCarouselTwo).length) {
            $(cardCarouselTwo).owlCarousel({
                loop: true,
                items: 4,
                nav: false,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                margin: 30,
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1
                    },
                    // breakpoint from 600 up
                    600 : {
                        items: 3
                    },
                    // breakpoint from 1200 up
                    1200 : {
                        items: 4
                    }
                }
            });
        }

        /*==== card-carousel-3 =====*/
        if ($(cardCarouselThree).length) {
            $(cardCarouselThree).owlCarousel({
                loop: true,
                items: 2,
                nav: false,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                margin: 30,
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1
                    },
                    // breakpoint from 600 up
                    600 : {
                        items: 2
                    }
                }
            });
        }

        /*==== Client logo =====*/
        if ($(clientLogoCarousel).length) {
            $(clientLogoCarousel).owlCarousel({
                loop: true,
                items: 6,
                nav: false,
                dots: false,
                smartSpeed: 700,
                autoplay: true,
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1
                    },
                    // breakpoint from 425 up
                    425 : {
                        items: 2
                    },
                    // breakpoint from 480 up
                    480 : {
                        items: 2
                    },
                    // breakpoint from 767 up
                    767 : {
                        items: 4
                    },
                    // breakpoint from 992 up
                    992 : {
                        items: 6
                    }
                }
            });
        }

        /*==== testimonial-carousel =====*/
        if ($(testimonialCarousel).length) {
            $(testimonialCarousel).owlCarousel({
                loop: true,
                items: 3,
                center: true,
                nav: true,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                margin: 10,
                navText: ["<i class=\"la la-arrow-left\"></i>", "<i class=\"la la-arrow-right\"></i>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1
                    },
                    // breakpoint from 768 up
                    768 : {
                        items: 2
                    },
                    // breakpoint from 992 up
                    992 : {
                        items: 3
                    }
                }
            });
        }

        /*==== gallery-carousel =====*/
        if ($(galleryCarousel).length) {
            $(galleryCarousel).owlCarousel({
                loop: true,
                items: 1,
                nav: true,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                dotsData: true,
                navText: ["<span class=\"la la-chevron-left\"></span>", "<span class=\"la la-chevron-right\"></span>"]
            });
        }

        /*==== Date dropper picker =====*/
        if ($(dateDropperPicker).length) {
            $(dateDropperPicker).dateDropper({
                format: 'm/d/Y',
                theme: 'vanilla',
                roundtrip: 'my-trip',
                large: true,
                largeDefault: true,
            })
        }

        if ($(datePicker).length) {
            $(datePicker).dateDropper({
                format: 'm/d/Y',
                theme: 'vanilla',
                large: true,
                largeDefault: true,
            })
        }
        
        /*==== Full screen slider =====*/
        if ($(fullScreenSlider).length) {
            $(fullScreenSlider).owlCarousel({
                loop: false,
                items: 4,
                nav: true,
                dots: false,
                smartSpeed: 700,
                autoplay: false,
                margin: 5,
                navText: ["<span class=\"la la-arrow-left\"></span>", "<span class=\"la la-arrow-right\"></span>"],
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: 1,
                        autoplay: true,
                    },
                    // breakpoint from 768 up
                    768 : {
                        items: 2,
                        autoplay: true,
                    },
                    // breakpoint from 992 up
                    992 : {
                        items: 4
                    }
                }
            });
        }

        /*==== Online user slider =====*/
        if ($(onlineUserSlider).length) {
            $(onlineUserSlider).owlCarousel({
                loop: false,
                items: 4,
                nav: true,
                dots: false,
                smartSpeed: 700,
                autoplay: false,
                margin: 5,
                navText: ["<span class=\"la la-angle-left\"></span>", "<span class=\"la la-angle-right\"></span>"],
            });
        }

        /*==== Single slider =====*/
        if ($(singleSlider).length) {
            $(singleSlider).owlCarousel({
                loop: true,
                items: 1,
                nav: true,
                dots: true,
                smartSpeed: 700,
                autoplay: false,
                navText: ["<span class=\"la la-angle-left\"></span>", "<span class=\"la la-angle-right\"></span>"]
            });
        }

        /*==== Quantity number =====*/
        $('.qtyDec, .qtyInc').on('click', function () {
            var $this = $(this);
            var oldValue = $this.parent().find('input[type="text"]').val();

            if ($this.hasClass('qtyInc')) {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                // don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            $this.parent().find('input[type="text"]').val(newVal);
        });

        /*==== Chosen select =====*/
        if ($(userChosenSelect).length) {
            $(userChosenSelect).chosen({
                no_results_text: "Oops, nothing found!",
                allow_single_deselect: true
            });
        }

        $(userChosenSelect).on('chosen:showing_dropdown', function(event, params) {
            var chosen_container = $( event.target ).next( '.chosen-container' );
            var dropdown = chosen_container.find( '.chosen-drop' );
            var dropdown_top = dropdown.offset().top - $(window).scrollTop();
            var dropdown_height = dropdown.height();
            var viewport_height = $(window).height();

            if ( dropdown_top + dropdown_height > viewport_height ) {
                chosen_container.addClass( 'chosen-drop-up' );
            }
        });

        $(userChosenSelect).on('chosen:hiding_dropdown', function(event, params) {
            $( event.target ).next( '.chosen-container' ).removeClass( 'chosen-drop-up' );
        });

        /*==== Counter up =====*/
        if ($(numberCounter).length) {
            $(numberCounter).counterUp({
                delay: 10,
                time: 1000
            });
        }

        /*==== jqte text editor =====*/
        if ($(userTextEditor).length) {
            $(userTextEditor).jqte({
                //placeholder: "Detail description about of your listing",
                formats: [
                    ["p","Paragraph"],
                    ["h1","Heading 1"],
                    ["h2","Heading 2"],
                    ["h3","Heading 3"],
                    ["h4","Heading 4"],
                    ["h5","Heading 5"],
                    ["h6","Heading 6"],
                    ["pre","Preformatted"]
                ]
            });
        }

        /*==== Open now filter btn =====*/
        $document.on('click', '.open-filter-btn', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
        });

        /*==== js-tilt =====*/
        if ($(jsTilt).length) {
            $(jsTilt).tilt({
                maxTilt: 1,
            });
        }
        if ($(jsTiltTwo).length) {
            $(jsTiltTwo).tilt({
                maxTilt: 4,
            });
        }

        /*==== data-fancybox =====*/
        $('[data-fancybox]').fancybox();


        /*==== emoji-picker =====*/
        if ($(emojiPicker).length) {
            $(emojiPicker).emojioneArea({
                pickerPosition: "top"
            });
        }

        /*====  Tooltip =====*/
        $('[data-toggle="tooltip"]').tooltip();

        /*======= ui price range slider ========*/
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 50, 290 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
            " - $" + $( "#slider-range" ).slider( "values", 1 ) );

        /*=========== bookmark btn ============*/
        $document.on('click', '.bookmark-btn', function () {
            $(this).toggleClass('active');
        });

        /*=========== modal ============*/
        $document.on('click', '.login-btn', function () {
            $('.login-form').modal('show');
            $('.signup-form, .message-form').modal('hide');
        });
        $document.on('click', '.signup-btn', function () {
            $('.login-form, .recover-form').modal('hide');
            $('.signup-form').modal('show');
        });
        $document.on('click', '.lost-pass-btn', function () {
          //  $('.login-form').modal('hide');
          //  $('.recover-form').modal('show');
        });

        $(".login-form").on("hidden.bs.modal", function () {
            // put your default event here
            $(".alertbox").css("display", "none");
            $(".form_error").css("display", "none");
            $('#emailError').text("");
            $('#passwordError').text("");

        });

        $(".signup-form").on("hidden.bs.modal", function () {
            // put your default event here
            $(".alertbox").css("display", "none");
            $(".form_error").css("display", "none");
            $('#formRegisterError').text("");

        });

        /*=========== Google map ============*/
        if($("#map").length) {
            initMap('map', 40.717499, -74.044113, 'images/map-marker.png');
        }

        /*======== Rating Overview ===========*/
        function ratingOverview(ratingElem) {

            $(ratingElem).each(function() {
                var dataRating = $(this).attr('data-rating');

                // Rules
                if (dataRating >= 4.0) {
                    $(this).addClass('high');
                    $(this).find('.review-bars-review-inner').css({ width: (dataRating/5)*100 + "%", });
                } else if (dataRating >= 3.0) {
                    $(this).addClass('mid');
                    $(this).find('.review-bars-review-inner').css({ width: (dataRating/5)*80 + "%", });
                } else if (dataRating < 3.0) {
                    $(this).addClass('low');
                    $(this).find('.review-bars-review-inner').css({ width: (dataRating/5)*60 + "%", });
                }

            });
        } ratingOverview('.review-bars-review');

        $(window).on('resize', function() {
            ratingOverview('.review-bars-review');
        });

        /*=========== Payment Method Accordion ============*/
        var radios = document.querySelectorAll('.payment-tab-toggle > input');

        for (var i = 0; i < radios.length; i++) {
            radios[i].addEventListener('change', expandAccordion);
        }

        function expandAccordion (event) {
            var allTabs = document.querySelectorAll('.payment-tab');
            for (var i = 0; i < allTabs.length; i++) {
                allTabs[i].classList.remove('is-active');
            }
            event.target.parentNode.parentNode.classList.add('is-active');
        }

        /*========== Page scroll ==========*/
        scrollLink.on('click',function(e){
            var target = $($(this).attr('href'));

            $($dom).animate({
                scrollTop:target.offset().top
            },600);

            $(this).addClass('active');

            e.preventDefault();
        });

        function findPosition (){
            $('.page-scroll').each(function(){
                if(($(this).offset().top - $(window).scrollTop()) < 20){
                    scrollLink.removeClass('active');
                    $('#sticky-content-nav').find('[data-scroll="'+ $(this).attr('id') +'"]').addClass('active');
                }
            });
        }

        /*========== Lazy loading ==========*/
        if ($(lazyLoading).length) {
            $(lazyLoading).Lazy();
        }


        /*========== USER LOGIN ==========*/

        $("#loginButton").on("click", function(e) {
            e.preventDefault();
            console.log("FORM SENDING")

            $(".alertbox").css("display", "none");
            $(".form_error").css("display", "none");
            $('#emailError').text("")
            $('#passwordError').text("")
            
            var loginInfo = {
                username: $.trim($("#loginUsername").val()),
                password: $.trim($("#loginPassword").val()),
                _token: $.trim($("#_token1").val()),
              };
            console.log(loginInfo)
            if ((loginInfo.username === '') && (loginInfo.password === '')) {
                $(".alertbox").show()
                $(".form_error").show()
                $('#passwordError').text("Password cannot be blank").show();
                $('#emailError').text("Username cannot be blank").show();
                return;
            } else if (loginInfo.password === '') {
                $(".alertbox").show()
                $(".form_error").show()
                 $('#passwordError').text("Password cannot be blank").show();
                return;
            } else if (loginInfo.username === '') {
                $(".alertbox").show()
                $(".form_error").show()
                $('#emailError').text("Username cannot be blank").show();
                return;
            }
            console.log("continue")

            // add spinner

            var $this = $(this);
            var ohtml = $this.html();

            var nhtml = "<span class='spinner-grow spinner-grow-sm' role='status' aria-hidden='true'></span> " + this.dataset.buttonSpinner;
            $this.html(nhtml);
            $this.attr("disabled", true);
            $('#loginUsername').attr("disabled", true);
            $('#loginPassword').attr("disabled", true);

            $.ajax({
                type: "POST",
                url: location.protocol + "//" + location.host +"/login",
                dataType: "json",
                data: loginInfo,
            
              }).done(function(data) {
                $('#loginButton').html(ohtml);
                $('#loginButton').attr("disabled", false);
                $('#loginUsername').attr("disabled", false);
                $('#loginPassword').attr("disabled", false);

                var status = data.status 
                if(status === 'error'){
                    var message = data.error
                    $(".alertbox").show()
                    $(".form_error").show()
                    $('#passwordError').text(message).show();
                }else{
                    document.location.reload()
                }
              }).fail(function(xhr) {
            
              })
        });

        /*========== USER REGISTER ==========*/

        $("#registerButton").on("click", function(e) {
            e.preventDefault();
            console.log("FORM SENDING")

            $(".alertbox").css("display", "none");
            $(".form_error").css("display", "none");
            $('#formRegisterError').text("")
            
            var registerInfo = {
                username: $.trim($("#registerUsername").val()),
                email: $.trim($("#registerEmail").val()),
                password: $.trim($("#registerPassword").val()),
                _token: $.trim($("#_token2").val()),
              };

            var confirmPassword = $.trim($("#confirmPassword").val())

            console.log(registerInfo)
            var err = false
            var errText = []
            var regex = /^[a-zA-Z0-9.\-_$@*!]{3,30}$/;

            if(!regex.test(registerInfo.username ) ){
                errText.push("Username format is wrong")
                err = true
            }
            if ((registerInfo.username === '')) {
                errText.push("Username cannot be blank")
                err = true
            } 
            if (registerInfo.password === '') {
                errText.push("Password cannot be blank")
                err = true
            } 
            if (registerInfo.password !== confirmPassword) {
                errText.push("Please confirm password")
                err = true
            } 
            if (registerInfo.email === '') {
                errText.push("Email cannot be blank")
                err = true
            }

            if(err){
                var errorText = errText.join('<br/>')
                $(".alertbox").show()
                $(".form_error").show()
                $('#formRegisterError').html(errorText).show();

                return;
            }
            console.log("continue")

            // add spinner

            var $this = $(this);
            var ohtml = $this.html();

            var nhtml = "<span class='spinner-grow spinner-grow-sm' role='status' aria-hidden='true'></span> " + this.dataset.buttonSpinner;
            $this.html(nhtml);
            $this.attr("disabled", true);
            $('#registerUsername').attr("disabled", true);
            $('#registerPassword').attr("disabled", true);
            $('#registerEmail').attr("disabled", true);

            $.ajax({
                type: "POST",
                url: location.protocol + "//" + location.host +"/register",
                dataType: "json",
                data: registerInfo,
            
              }).done(function(data) {
                $('#registerButton').html(ohtml);
                $('#registerButton').attr("disabled", false);
                $('#registerUsername').attr("disabled", false);
                $('#registerPassword').attr("disabled", false);
                $('#registerEmail').attr("disabled", false);

                var status = data.status 
                if(status === 'error'){
                    var message = data.error
                    $(".alertbox").show()
                    $(".form_error").show()
                    $('#formRegisterError').html(message).show();
                    return
                }else{
                    document.location.reload()
                }
              }).fail(function(xhr) {
            
              })
        });


        $(".serviceDetailContainer").hide()
        $("#serviceType").on("change", function(e) {
            var value = this.value
            $(".serviceDetailContainer").hide()
            if(value == 'Animal Service'){
                //class="serviceDetail" id="serviceDetail1"
                $("#serviceDetailContainer1").show()
            }
            if(value == 'Beauty & Wellness'){
                $("#serviceDetailContainer2").show()
            }
            if(value == 'Transport'){
                $("#serviceDetailContainer3").show()
            }
            if(value == 'Entertainment'){
                $("#serviceDetailContainer4").show()
            }
            if(value == 'Home Service'){
                $("#serviceDetailContainer5").show()
            }
            if(value == 'Cleaning Service'){
                $("#serviceDetailContainer6").show()
            }
            if(value == 'Personal Trainer'){
                $("#serviceDetailContainer7").show()
            }
            if(value == 'Teacher'){
                $("#serviceDetailContainer8").show()
            }

        });
        
        selectCategory.on("change", function(e) {
            hideServiceDetail();
            $("#subcategory_service").hide();
            $("#subcategory_accommodation").hide();
            $("#service_detail").hide();
            hideServiceDetail();  
            if(this.value =='accommodation'){
                $("#subcategory_accommodation").show();
            }
            if(this.value =='service'){
                $("#subcategory_service").show();
                $("#service_detail").show();
                checkValue($("#service_detail").val())
            }

        });

        $('#subcategory_service_select').on("change", function(e) {
            hideServiceDetail();
            var value = this.value;
            checkValue(value)

        });     
       
        function checkValue(value){
            if(value =='Animal Service'){
                $("#service_animal").show();
            }
            if(value =='Beauty & Wellness'){
                $("#service_beauty").show();
            }
            if(value =='Entertainment'){
                $("#service_entertainment").show();
            }
            if(value =='Home Service'){
                $("#service_home").show();
            }
            if(value =='Cleaning Service'){
                $("#service_cleaning").show();
            }
            if(value =='Personal Trainer'){
                $("#service_trainer").show();
            }
            if(value =='Teacher'){
                $("#service_teacher").show();
            }
            if(value =='Transport'){
                $("#service_driver").show();
            }
        }
        function hideServiceDetail(){
            $('#service_animal').hide()
            $('#service_beauty').hide()
            $('#service_driver').hide()
            $('#service_entertainment').hide()
            $('#service_home').hide()
            $('#service_cleaning').hide()
            $('#service_trainer').hide()
            $('#service_teacher').hide()
            
        }
       
    });

    /** TOP UP FUNCTION */

    $("#topupbutton").on("click", function(e) {
        e.preventDefault();
        console.log("FORM SENDING")
        var file_data = $("#paymentproof").prop("files")[0];   
        var amount = $("#paymentamount").val();   
        var error_message = ""
        var error = false
        $(".alertbox").css("display", "none");
        $(".form_error").css("display", "none");
        if(amount == ""){
            error_message += "Please insert amount"
            $(".alertbox").show()
            $(".form_error").show()
            $('#formerror').html(error_message).show();
            error = true
        }else if(amount < 500000){
            error_message += "Minimum amount is IDR 500.000 <br/>"
            $(".alertbox").show()
            $(".form_error").show()
            $('#formerror').html(error_message).show();
            error = true
        }
        if(file_data === undefined){
            error_message += "Please insert proof of payment"
            $(".alertbox").show()
            $(".form_error").show()
            $('#formerror').html(error_message).show();
            error = true
        }
        if(error){
            return;
        }
         // add spinner

         var $this = $(this);
         var ohtml = $this.html();
 
         var nhtml = "<span class='spinner-grow spinner-grow-sm' role='status' aria-hidden='true'></span> " + this.dataset.buttonSpinner;
         $this.html(nhtml);
         $this.attr("disabled", true);
         $('#paymentamount').attr("disabled", true);
         var form_data = new FormData();                  
        form_data.append('file', file_data);
        form_data.append('amount', amount);
        form_data.append('_token', $("#_token1").val());
 
         $.ajax({
             type: "POST",
             url: location.protocol + "//" + location.host +"/dashboard/topup/",
             dataType: "json",
             data: form_data,
             cache: false,
             contentType: false,
             processData: false,
         
           }).done(function(data) {
            console.log(data)
           
             $('#topupbutton').html(ohtml);
             $('#topupbutton').attr("disabled", false);
             $('#paymentamount').attr("disabled", false);
 
             var status = data.status 
             if(status === 'error'){
                var message = data.error
                error_message = message
                $(".alertbox").show()
                $(".form_error").show()
                $('#formerror').html(error_message).show();
             }else{
                 document.location.reload()
             }
           }).fail(function(xhr) {
         
           })

    });

    /** WITHDRAW FUNCTION */

    $("#withdrawbutton").on("click", function(e) {
        e.preventDefault();
        console.log("FORM SENDING")
        var amount = $("#withdrawamount").val();   
        var error_message = ""
        var error = false
        $(".alertbox").css("display", "none");
        $(".form_error").css("display", "none");
        if(amount == ""){
            error_message += "Please insert amount"
            $(".alertbox").show()
            $(".form_error").show()
            $('#formerror2').html(error_message).show();
            error = true
        }else if(amount < 500000){
            error_message += "Minimum amount is IDR 500.000 <br/>"
            $(".alertbox").show()
            $(".form_error").show()
            $('#formerror2').html(error_message).show();
            error = true
        }
       
        if(error){
            return;
        }
         // add spinner

         var $this = $(this);
         var ohtml = $this.html();
 
         var nhtml = "<span class='spinner-grow spinner-grow-sm' role='status' aria-hidden='true'></span> " + this.dataset.buttonSpinner;
         $this.html(nhtml);
         $this.attr("disabled", true);
         $('#paymentamount').attr("disabled", true);
         var form_data = new FormData();                  
        form_data.append('amount', amount);
        form_data.append('_token', $("#_token1").val());
 
         $.ajax({
             type: "POST",
             url: location.protocol + "//" + location.host +"/dashboard/withdraw/",
             dataType: "json",
             data: form_data,
             cache: false,
             contentType: false,
             processData: false,
         
           }).done(function(data) {
            console.log(data)
           
             $('#withdrawbutton').html(ohtml);
             $('#withdrawbutton').attr("disabled", false);
             $('#withdrawamount').attr("disabled", false);
 
             var status = data.status 
             if(status === 'error'){
                var message = data.error
                error_message = message
                $(".alertbox").show()
                $(".form_error").show()
                $('#formerror2').html(error_message).show();
                $('#withdrawbutton').html(ohtml);
                $('#withdrawbutton').attr("disabled", false);
                $('#withdrawamount').attr("disabled", false);
             }else{
                 document.location.reload()
             }
           }).fail(function(xhr) {
         
           })

    });
    
    $(".topup-link").on("click", function(e) {
        $("#topup-page li a").removeClass('page-link-active');
        $(this).addClass('page-link-active');
        var page = $(this).html();
        $.ajax({
            type: "GET",
            url: location.protocol + "//" + location.host +"/dashboard/finance/?table=topup&page="+page,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
        
          }).done(function(data) {
            var dataTable = data.data;
            var tr="";
            for(var i = 0; i < dataTable.length; i ++){
                var dateParts = dataTable[i].created_at.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var Months = ['01','02','03','04','05','06','07','08','09','10','11','12']
                var tanggal = jsDate.getDate()+ "-"+Months[jsDate.getMonth()]+"-"+jsDate.getFullYear()
                var file = location.protocol + "//" + location.host +  dataTable[i].file_path
                var link = '<a href="'+file+'" target="_blank">Proof of Payment</a>'
                var status = dataTable[i].status
                var amount = dataTable[i].amount
                var description = dataTable[i].description
                tr+="<tr> <td>"+tanggal+"</td> <td>IDR "+amount+"</td> <td>"+link+"</td> <td>"+status+"</td> <td>"+description+"</td> </tr>"
               
            }
            $("#body-topup").html(tr)
          }).fail(function(xhr) {
        
          })
    });   

    $(".cashout-link").on("click", function(e) {
        $("#cashout-page li a").removeClass('page-link-active');
        $(this).addClass('page-link-active');
        var page = $(this).html();
        $.ajax({
            type: "GET",
            url: location.protocol + "//" + location.host +"/dashboard/finance/?table=cashout&page="+page,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
        
          }).done(function(data) {
            var response = data;
            var data1 = response[0];
            var data2 = response[1];
            var dataTable = data1.data
            var dataTable2 = data2.data

            var tr="";
            for(var i = 0; i < dataTable.length; i ++){
                var dateParts = dataTable[i].created_at.split("-");
                var jsDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2].substr(0,2));
                var Months = ['01','02','03','04','05','06','07','08','09','10','11','12']
                var tanggal = jsDate.getDate()+ "-"+Months[jsDate.getMonth()]+"-"+jsDate.getFullYear()
                var link = '';
                if(dataTable2[i].file_path !== null){
                    var file = location.protocol + "//" + location.host +  dataTable2[i].file_path
                    link = '<a href="'+file+'" target="_blank">Proof of Payment</a>'
                }else{
                    link = '-';
                }
               
                var status = dataTable[i].status
                var amount = dataTable[i].amount
                var description = dataTable[i].description
                tr+="<tr> <td>"+tanggal+"</td> <td>IDR "+amount+"</td> <td>"+link+"</td> <td>"+status+"</td> <td>"+description+"</td> </tr>"
               
            }
            $("#body-cashout").html(tr)
          }).fail(function(xhr) {
        
          })
    });   

    


})(jQuery);

