@extends('layouts.main')

@section('content')
    <!-- ================================
                                                        START BREADCRUMB AREA
                                                    ================================= -->
    <section class="breadcrumb-area bread-bg-accommodations bread-overlay overflow-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-content d-flex flex-wrap align-items-center justify-content-between">
                        <div class="section-heading">
                            <h2 class="sec__title text-white font-size-40 mb-0">Listings</h2>
                        </div>
                        <ul class="list-items bread-list ">
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li>Listings</li>
                        </ul>
                    </div><!-- end breadcrumb-content -->
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
        </div><!-- end container -->
        <div class="bread-svg">
            <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="50px" viewBox="0 0 1200 150"
                preserveAspectRatio="none">
                <g>
                    <path fill-opacity="0.2"
                        d="M0,150 C600,100 1000,50 1200,-1.13686838e-13 C1200,6.8027294 1200,56.8027294 1200,150 L0,150 Z">
                    </path>
                </g>
                <g class="pix-waiting animated" data-anim-type="fade-in-up" data-anim-delay="300">
                    <path fill="rgba(255,255,255,0.8)"
                        d="M0,150 C600,120 1000,80 1200,30 C1200,36.8027294 1200,76.8027294 1200,150 L0,150 Z"></path>
                </g>
                <path fill="#fff" d="M0,150 C600,136.666667 1000,106.666667 1200,60 C1200,74 1200,104 1200,150 L0,150 Z">
                </path>
                <defs></defs>
            </svg>
        </div><!-- end bread-svg -->
    </section><!-- end breadcrumb-area -->
    <!-- ================================
                                                        END BREADCRUMB AREA
                                                    ================================= -->

    <!-- ================================
                                                        START CARD AREA
                                                    ================================= -->
    <section class="card-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                        @if ($listing ?? 0)
                            @foreach ($listing ?? '' as $list)
                                <div class="col-lg-6 responsive-column">
                                    @php
                                        $images = $list->images;
                                        $main_image = '';
                                        if ($images) {
                                            foreach ($images as $image) {
                                                if ($image->type == 'main_image') {
                                                    $main_image = $image->file_path;
                                                }
                                            }
                                        }
                                    @endphp
                                    <div class="card-item">
                                        <div class="card-content">
                                            @if ($main_image != '')
                                                <a href="/"><img class="card-img-top" src="/" /></a>
                                            @endif
                                            <h4 class="card-title pt-3">
                                                <a href="/">/</a>
                                            </h4>
                                            <ul class="info-list padding-top-20px">
                                                <li class="d-flex align-items-center">
                                                    <span class="rate-text">0 Reviews</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div><!-- end card-item -->
                                </div><!-- end col-lg-6 -->
                            @endforeach

                    </div><!-- end row -->

                    <!-- PAGINATION -->

                    <div class="row">
                        <div class="col-lg-12 pt-3 text-center">
                            <div class="pagination-wrapper d-inline-block">
                                <div class="section-pagination">
                                    <nav aria-label="Page navigation">
                                        <ul class="pagination flex-wrap justify-content-center">

                                        </ul>
                                    </nav>
                                </div><!-- end section-pagination -->
                                <ul class="pagination-simple d-flex align-items-center justify-content-center pt-3">

                                </ul>
                            </div>
                        </div><!-- end col-lg-12 -->
                    @else
                        <div class="row">
                            <div class="col-lg-12 pt-3 text-center">
                                <h2>no result found</h2>
                            </div>
                        </div>
                        @endif
                    </div><!-- end row -->

                </div><!-- end col-lg-8 -->
                <!-- ================================
                                                                    START SEARCH FILTER AREA
                                                                ================================= -->

                <div class="col-lg-4">
                    <div class="sidebar mt-0">
                        <div class="sidebar-widget">
                            <h3 class="widget-title">Search</h3>
                            <div class="stroke-shape mb-4"></div>
                            <form action="/" class="form-box">
                                <div class="form-group user-chosen-select-container">
                                    <select class="user-chosen-select" name="country">
                                        <option value="ID">Indonesia</option>
                                    </select>
                                </div><!-- end form-group -->
                                <div class="form-group user-chosen-select-container">
                                    <select class="user-chosen-select" name="city">
                                        <!-- <option value="city">Select a City</option>         -->
                                        <option value="denpasar">Bali</option>
                                    </select>
                                </div><!-- end form-group -->
                                <div class="form-group user-chosen-select-container">
                                    <select class="user-chosen-select" name="category" id="select-category">
                                        <option value="category">Select a Category</option>
                                        <option value="accommodation">Accommodation</option>
                                        <option value="service">Service</option>
                                    </select>
                                </div><!-- end form-group -->
                                <div class="form-group user-chosen-select-container">
                                    <div id="subcategory_service">
                                        <select class="user-chosen-select" name="subcategory_service"
                                            id="subcategory_service_select">
                                            <option value="all">Select a Sub-Category</option>

                                            <!---------- SHOW SERVICE SUB-CATEGORIES WHEN SERVICE SELECTED ---------->

                                            <option value="Animal Service">Animal Service</option>
                                            <option value="Beauty & Wellness">Beauty & Wellness</option>
                                            <option value="Entertainment">Entertainment</option>
                                            <option value="Home Service">Home Service</option>
                                            <option value="Cleaning Service">Cleaning Service</option>
                                            <option value="Personal Trainer">Personal Trainer</option>
                                            <option value="Teacher">Teacher</option>
                                            <option value="Transport">Transport</option>
                                        </select>
                                    </div>
                                    <div id="subcategory_accommodation">
                                        <select class="user-chosen-select" name="subcategory_accommodation">
                                            <option value="Apartment">Apartment</option>
                                            <option value="Bed & Breakfast">Bed & Breakfast</option>
                                            <option value="Boutique Hotel">Boutique Hotel</option>
                                            <option value="Business Hotel">Business Hotel</option>
                                            <option value="Campsite">Campsite</option>
                                            <option value="Capsule Hotel">Capsule Hotel</option>
                                            <option value="Cave Hotel">Cave Hotel</option>
                                            <option value="Co-working Space">Co-working Space</option>
                                            <option value="Cottage">Cottage</option>
                                            <option value="Farmhouse">Farmhouse</option>
                                            <option value="Finca">Finca</option>
                                            <option value="Guesthouse">Guesthouse</option>
                                            <option value="Hostel">Hostel</option>
                                            <option value="Hotel">Hotel</option>
                                            <option value="House">House</option>
                                            <option value="Inn">Inn</option>
                                            <option value="Lodge">Lodge</option>
                                            <option value="Motel">Motel</option>
                                            <option value="Pension">Pension</option>
                                            <option value="Penthouse">Penthouse</option>
                                            <option value="Private Island Resort">Private Island Resort</option>
                                            <option value="Resort">Resort</option>
                                            <option value="Townhouse">Townhouse</option>
                                            <option value="Treehouse">Treehouse</option>
                                            <option value="Villa">Villa</option>
                                            <option value="Yurt">Yurt</option>
                                        </select>
                                    </div>
                                </div><!-- end form-group -->

                                <div class="form-group user-chosen-select-container" id="service_detail">
                                    <div id="service_animal">
                                        <select class="user-chosen-select" name="service_detail_animal">
                                            <option value="all">Select Service Detail</option>

                                            <!---------- SHOW SERVICE DETAIL WHEN SERVICE SUB-CATEGORY SELECTED OTHERWISE HIDE ---------->

                                            <!---------- SHOW IF SUB-CATEGORY = ANIMAL SERVICE ---------->
                                            <option value="Animal hairdresser">Animal hairdresser</option>
                                            <option value="Animal sitting">Animal sitting</option>
                                            <option value="Dog walking">Dog walking</option>
                                        </select>
                                    </div>
                                    <div id="service_beauty">
                                        <select class="user-chosen-select" name="service_detail_beauty">
                                            <!---------- SHOW IF SUB-CATEGORY = BEAUTY & WELLNESS ---------->
                                            <option value="all">Select Service Detail</option>
                                            <option value="Haircare">Haircare</option>
                                            <option value="Makeup">Makeup</option>
                                            <option value="Manicure">Manicure</option>
                                            <option value="Massage">Massage</option>
                                            <option value="Pedicure">Pedicure</option>
                                            <option value="Skincare">Skincare</option>
                                            <option value="Waxing">Waxing</option>
                                        </select>
                                    </div>
                                    <!---------- SHOW IF SUB-CATEGORY = DRIVER ---------->
                                    <div id="service_driver">
                                        <select class="user-chosen-select" name="service_detail_transport">
                                            <option value="all">Select Service Detail</option>
                                            <option value="Airport pickup">Airport pickup</option>
                                            <option value="Beer bus">Beer bus</option>
                                            <option value="Bus">Bus</option>
                                            <option value="Car">Car</option>
                                            <option value="Daytour">Daytour</option>
                                        </select>
                                    </div>
                                    <!---------- SHOW IF SUB-CATEGORY = ENTERTAINMENT ---------->
                                    <div id="service_entertainment">
                                        <select class="user-chosen-select" name="service_detail_entertainment">
                                            <option value="all">Select Service Detail</option>
                                            <option value="Comedian">Comedian</option>
                                            <option value="Dancer">Dancer</option>
                                            <option value="DJ">DJ</option>
                                            <option value="Magician">Magician</option>
                                            <option value="Master of ceremony">Master of ceremony</option>
                                            <option value="Model">Model</option>
                                            <option value="Music artist">Music artist</option>
                                            <option value="Photographer">Photographer</option>
                                            <option value="Tourguide">Tourguide</option>
                                            <option value="Videographer">Videographer</option>
                                        </select>
                                    </div>
                                    <div id="service_home">
                                        <select class="user-chosen-select" name="service_detail_home">
                                            <!---------- SHOW IF SUB-CATEGORY = HOME SERVICE ---------->
                                            <option value="all">Select Service Detail</option>
                                            <option value="AC maintenance">AC maintenance</option>
                                            <option value="Chef">Chef</option>
                                            <option value="Gardener">Gardener</option>
                                            <option value="Handyman">Handyman</option>
                                            <option value="Mover & packer">Mover & packer</option>
                                            <option value="Nanny">Nanny</option>
                                            <option value="Maid">Maid</option>
                                            <option value="Painter">Painter</option>
                                        </select>
                                    </div>
                                    <div id="service_cleaning">
                                        <select class="user-chosen-select" name="service_detail_cleaning">
                                            <!---------- SHOW IF SUB-CATEGORY = CLEANING SERVICE ---------->
                                            <option value="all">Select Service Detail</option>
                                            <option value="Carpet cleaning">Carpet cleaning</option>
                                            <option value="Curtain cleaning">Curtain cleaning</option>
                                            <option value="House cleaning">House cleaning</option>
                                            <option value="Mattress cleaning">Mattress cleaning</option>
                                            <option value="Office cleaning">Office cleaning</option>
                                            <option value=">Pool cleaning">Pool cleaning</option>
                                            <option value="Room cleaning">Room cleaning</option>
                                            <option value="Sofa cleaning">Sofa cleaning</option>
                                            <option value="Water tank cleaning">Water tank cleaning</option>
                                            <option value="Window cleaning">Window cleaning</option>
                                        </select>
                                    </div>
                                    <div id="service_trainer">
                                        <select class="user-chosen-select" name="service_detail_trainer">
                                            <!---------- SHOW IF SUB-CATEGORY = PERSONAL TRAINER ---------->
                                            <option value="all">Select Service Detail</option>
                                            <option value="Bodybuilding">Bodybuilding</option>
                                            <option value="Diving">Diving</option>
                                            <option value="Endurance">Endurance</option>
                                            <option value="Group fitness">Group fitness</option>
                                            <option value="Material arts">Material arts</option>
                                            <option value="Nutritional advice">Nutritional advice</option>
                                            <option value="Strength">Strength</option>
                                            <option value="Surfing">Surfing</option>
                                            <option value="Yoga">Yoga</option>
                                            <option value="Zumba">Zumba</option>
                                        </select>
                                    </div>
                                    <div id="service_teacher">
                                        <select class="user-chosen-select" name="service_detail_teacher">
                                            <option value="all">Select Service Detail</option>
                                            <!---------- SHOW IF SUB-CATEGORY = TEACHER ---------->
                                            <option value="Biology">Biology</option>
                                            <option value="Chemistry">Chemistry</option>
                                            <option value="History">History</option>
                                            <option value="Language">Language</option>
                                            <option value="Math">Math</option>
                                            <option value="Music">Music</option>
                                            <option value="Physics">Physics</option>
                                        </select>
                                    </div>
                                </div><!-- end form-group -->
                                <div class="btn-box">
                                    <button type="submit" class="orange-theme-btn button-orange border-0 w-100 mt-3">
                                        <i class="la la-search mr-2"></i>Search Now
                                    </button>
                                </div><!-- end btn-box -->
                            </form>
                        </div><!-- end sidebar-widget -->
                    </div><!-- end sidebar -->
                </div><!-- end col-lg-4 -->
                <!-- ================================ include_once 'listingfilter.php' ================================= -->
                <!-- ================================
                                                                    END SEARCH FILTER AREA
                                                                ================================= -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end card-area -->
    <!-- ================================
                                                        END CARD AREA
                                                    ================================= -->
    <script type="text/javascript">

    </script>
@endsection
