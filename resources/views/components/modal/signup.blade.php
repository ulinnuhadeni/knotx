<!-- SIGN-UP MODAL -->
<div class="modal fade modal-container signup-form" id="signUpModal" tabindex="-1" role="dialog"
    aria-labelledby="signUpModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center mh-bg-2">
                <h5 class="modal-title" id="signUpModalTitle">Sign up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-times-circle"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center padding-top-30px">
                    <div class="alert alert-danger alertbox" role="alert">
                        <p id="formRegisterError" class="form_error"></p>
                    </div>
                </div>
                <form class="form-box">
                    <input type="hidden" value="{{ csrf_token() }}" id="_token2" />
                    <div class="input-box">
                        <label class="label-text">Username</label>
                        <div class="form-group">
                            <span class="la la-user form-icon"></span>
                            <input class="form-control form-control-styled" type="text" name="username"
                                id="registerUsername" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="input-box">
                        <label class="label-text">Email</label>
                        <div class="form-group">
                            <span class="la la-envelope form-icon"></span>
                            <input class="form-control form-control-styled" type="email" name="email" id="registerEmail"
                                placeholder="Email address" required>
                        </div>
                    </div>
                    <div class="input-box">
                        <label class="label-text">Password</label>
                        <div class="form-group">
                            <span class="la la-lock form-icon"></span>
                            <input class="form-control form-control-styled" type="password" name="pwd"
                                id="registerPassword" placeholder="Enter password" required>
                        </div>
                    </div>
                    <div class="input-box">
                        <label class="label-text">Confirm Password</label>
                        <div class="form-group">
                            <span class="la la-lock form-icon"></span>
                            <input class="form-control form-control-styled" type="password" name="pwd2"
                                id="confirmPassword" placeholder="Confirm password" required>
                        </div>
                    </div>
                    <div class="input-box py-4 user-action-meta">
                        <div>
                            <p class="font-size-14 mt-n2">By selecting <strong>Agree and continue</strong> below, I
                                agree to Knot-X’s
                                <a href="help/terms&policies/terms-of-service.php" class="text-color-2">Terms of
                                    Service</a> and
                                <a href="help/terms&policies/privacy-policy.php" class="text-color-2">Privacy
                                    Policy</a>.
                            </p>
                        </div>
                    </div>
                    <div class="btn-box">
                        <button type="submit" name="registeracc" id="registerButton"
                            class="orange-theme-btn button-orange border-0 w-100" data-button-spinner="loading">
                            <i class="la la-user-plus mr-1"></i> Agree and continue
                        </button>
                    </div>
                    <div class="text-center padding-top-30px">
                        <p class="sub-text-box text-left pt-1 font-weight-medium font-size-14">
                            Already have an account? <a class="text-color-2 login-btn" href="javascript:void(0)">Sign
                                in</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
