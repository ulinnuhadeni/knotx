 <!-- Sign In Modal -->
 <div class="modal fade modal-container login-form" id="loginModal" tabindex="-1" role="dialog"
     aria-labelledby="loginModalTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered" role="document">
         <div class="modal-content">
             <div class="modal-header align-items-center mh-bg">
                 <h5 class="modal-title" id="loginModalTitle">Sign in</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true" class="la la-times-circle"></span>
                 </button>
             </div>
             <div class="modal-body">
                 <div class="text-center padding-top-30px">
                     <div class="alert alert-danger alertbox" role="alert">
                         <p id="passwordError" class="form_error">a</p>
                         <p id="emailError" class="form_error"></p>
                     </div>
                 </div>
                 <form class="form-box">
                     <input type="hidden" value="{{ csrf_token() }}" id="_token1" />
                     <div class="input-box">
                         <label class="label-text">Username or email</label>
                         <div class="form-group">
                             <span class="la la-user form-icon"></span>
                             <input class="form-control form-control-styled" type="text" name="username"
                                 id="loginUsername" placeholder="Username or email address">
                         </div>
                     </div>
                     <div class="input-box">
                         <label class="label-text">Password</label>
                         <div class="form-group">
                             <span class="la la-lock form-icon"></span>
                             <input class="form-control form-control-styled" type="password" name="pwd"
                                 id="loginPassword" placeholder="Enter password">
                         </div>
                     </div>
                     <div class="input-box d-flex align-items-center justify-content-between pb-4 user-action-meta">
                         <div class="custom-checkbox">
                             <input type="checkbox" id="keepMeSignedChb">
                             <label for="keepMeSignedChb" class="font-size-14">Keep me signed in</label>
                         </div>
                         <a href="/" class="margin-bottom-10px lost-pass-btn font-size-14">Lost Password?</a>
                     </div>
                     <div class="btn-box">
                         <button name="signin" type="login" id="loginButton"
                             class="orange-theme-btn button-orange border-0 w-100" data-button-spinner="loading">
                             <i class="la la-sign-in mr-1"></i> Sign in
                         </button>
                     </div>
                     <div class="text-center padding-top-30px">
                         <p class="sub-text-box text-left pt-1 font-weight-medium font-size-14">
                             Don't have an account? <a class="text-color-2 signup-btn" href="javascript:void(0)">Create
                                 an account</a>
                         </p>
                     </div>
                 </form>
             </div>
         </div>
     </div>
 </div>
