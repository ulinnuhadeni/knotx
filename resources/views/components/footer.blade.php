 <!-- ================================
           START FOOTER AREA
    ================================= -->
 <section class="footer-area bg-gray pattern-bg-2 padding-bottom-30px position-relative">
     <span class="circle-bg circle-bg-1 position-absolute"></span>
     <span class="circle-bg circle-bg-2 position-absolute"></span>
     <span class="circle-bg circle-bg-3 position-absolute"></span>
     <div class="container">
         <div class="section-block-2 margin-bottom-40px"></div>
         <div class="row">
             <div class="col-lg-3 responsive-column">
                 <div class="footer-item">
                     <div class="footer-logo">
                         <a href="index.php" class="foot-logo"><img src="/assets/images/knot-x-long.png"
                                 alt="logo"></a>
                     </div><!-- end footer-logo -->
                 </div><!-- end footer-item -->
             </div><!-- end col-lg-3 -->
             <div class="col-lg-3 responsive-column">
                 <div class="footer-item">
                     <h4 class="footer__title">About</h4>
                     <div class="stroke-shape mb-3"></div>
                     <ul class="list-items">
                         <li><a href="how-knot-x-works.php">How Knot-X Works</a></li>
                         <li><a href="blog/blog-grid.php">Knot-X Blog</a></li>
                         <!-- <li><a href="career.php">Career</a></li> -->
                         <li><a href="konnex-agency.php">KonneX Agency</a></li>
                     </ul>
                 </div><!-- end footer-item -->
             </div><!-- end col-lg-3 -->
             <div class="col-lg-3 responsive-column">
                 <div class="footer-item">
                     <h4 class="footer__title">Provider</h4>
                     <div class="stroke-shape mb-3"></div>
                     <ul class="list-items">
                         <li><a href="badges.php">Badges</a></li>
                         <li><a href="knot-x-review.php">Knot-X Review</a></li>
                         <li><a href="superprovider.php">Superprovider</a></li>
                         <li><a href="premiumprovider.php">Premiumprovider</a></li>
                     </ul>
                 </div><!-- end footer-item -->
             </div><!-- end col-lg-3 -->
             <div class="col-lg-3 responsive-column">
                 <div class="footer-item">
                     <h4 class="footer__title">Community</h4>
                     <div class="stroke-shape mb-3"></div>
                     <ul class="list-items">
                         <li><a href="#">Help Center</a></li>
                         <li><a href="#">Trust & Safety</a></li>
                         <li><a href="#">Travel restrictions and advisories</a></li>
                     </ul>
                 </div><!-- end footer-item -->
             </div><!-- end col-lg-3 -->
         </div><!-- end row -->
         <div class="row pt-4 footer-action-wrap">
             <div class="col-lg-4">
                 <h4 class="font-size-17 pb-3">Follow us on</h4>
                 <ul class="social-profile social-profile-styled">
                     <li><a href="https://www.facebook.com/knotxonline/" class="facebook-bg" data-toggle="tooltip"
                             data-placement="top" title="Facebook" target="_blank" norefferal><i
                                 class="mdi mdi-facebook"></i></a></li>
                     <li><a href="https://twitter.com/knotxonline" class="twitter-bg" data-toggle="tooltip"
                             data-placement="top" title="Twitter" target="_blank" norefferal><i
                                 class="mdi mdi-twitter"></i></a></li>
                     <li><a href="https://www.instagram.com/knotxonline/" class="instagram-bg" data-toggle="tooltip"
                             data-placement="top" title="Instagram" target="_blank" norefferal><i
                                 class="mdi mdi-instagram"></i></a></li>
                     <li><a href="https://www.youtube.com/channel/UCc41g4akqDpEyOyn2Ecdq1Q" class="youtube-bg"
                             data-toggle="tooltip" data-placement="top" title="YouTube" target="_blank" norefferal><i
                                 class="mdi mdi-youtube"></i></a></li>
                 </ul>
             </div>
         </div>
         <div id="google_translate_element"></div>
         <div class="section-block-2 my-4"></div>
         <div class="row">
             <div class="col-lg-12">
                 <div class="copy-right d-flex align-items-center justify-content-between">
                     <p class="copy__desc">
                         &copy; Copyright Knot-X
                         <script>
                             document.write(new Date().getFullYear());
                         </script>. All rights reserved.
                     </p>
                     <ul class="list-items term-list text-right">
                         <li class="font-size-14"><a href="help/terms&policies/terms-and-policies.php">Terms &
                                 Policies</a></li>
                         <li class="font-size-14"><a href="help/terms&policies/privacy-policy.php">Privacy
                                 Policy</a></li>
                     </ul>
                 </div><!-- end copy-right -->
             </div><!-- end col-lg-12 -->
         </div><!-- end row -->
     </div><!-- end container -->
 </section>
 <!-- ================================
           END FOOTER AREA
    ================================= -->
