@php
$uniquecall1 = date('YmdHis');
@endphp

<!-- Template CSS Files -->
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/line-awesome.min.css">
<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/assets/css/animated-headline.css">
<link rel="stylesheet" href="/assets/css/jquery-ui.css">
<link rel="stylesheet" href="/assets/css/jquery.fancybox.css">
<link rel="stylesheet" href="/assets/css/chosen.min.css">
<link rel="stylesheet" href="/assets/css/jquery-te-1.4.0.css">
<link rel="stylesheet" href="/assets/css/jquery.fileupload.css">
<link rel="stylesheet" href="/assets/css/jquery.fileupload-ui.css">
<link rel="stylesheet" href="/assets/css/jquery.fileupload-noscript.css">
<link rel="stylesheet" href="/assets/css/jquery.fileupload-ui-noscript.css">
<link rel="stylesheet" href="/assets/css/style.css?uniquecall={{ $uniquecall1 }}">
