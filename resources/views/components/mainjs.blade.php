 <!-- Template JS Files -->
 <script src="/assets/js/jquery-ui.js"></script>
 <script src="/assets/js/jquery-3.4.1.min.js"></script>
 <script src="/assets/js/popper.min.js"></script>
 <script src="/assets/js/bootstrap.min.js"></script>
 <script src="/assets/js/owl.carousel.min.js"></script>
 <script src="/assets/js/jquery.fancybox.min.js"></script>
 <script src="/assets/js/animated-headline.js"></script>
 <script src="/assets/js/chosen.min.js"></script>
 <script src="/assets/js/moment.min.js"></script>
 <script src="/assets/js/datedropper.min.js"></script>
 <script src="/assets/js/waypoints.min.js"></script>
 <script src="/assets/js/jquery.counterup.min.js"></script>
 <script src="/assets/js/jquery-rating.js"></script>
 <script src="/assets/js/tilt.jquery.min.js"></script>
 <script src="/assets/js/jquery-supperslides.min.js"></script>
 <script src="/assets/js/jquery.lazy.min.js"></script>
 <script src="/assets/js/jquery-te-1.4.0.min.js"></script>
 <script src="/assets/js/jquery.MultiFile.min.js"></script>
 <script src="/assets/js/blueimp/js/tmpl.min.js"></script>
 <script src="/assets/js/blueimp/js/load-image.all.min.js"></script>
 <script src="/assets/js/blueimp/js/canvas-to-blob.min.js"></script>
 <script src="/assets/js/blueimp/js/jquery.iframe-transport.js"></script>
 <script src="/assets/js/blueimp/js/jquery.fileupload.js"></script>
 <script src="/assets/js/blueimp/js/jquery.fileupload-process.js"></script>
 <script src="/assets/js/blueimp/js/jquery.fileupload-image.js"></script>
 <script src="/assets/js/blueimp/js/jquery.fileupload-validate.js"></script>
 <script src="/assets/js/blueimp/js/jquery.fileupload-ui.js"></script>
 @php
     $uniquecall = date('YmdHis');
 @endphp
 <script src="/assets/knotx/js/main.js?uniqecall={{ $uniquecall }}"></script>
