   <!-- ================================
            START HEADER AREA
================================= -->
   <header class="header-area">
       <div class="header-menu-wrapper padding-right-30px padding-left-30px">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-lg-12">
                       <div class="menu-full-width">
                           <div class="logo">
                               <a href="/"><img src="/assets/images/knot-x-longw.png" alt="logo"></a>
                               <div class="d-flex align-items-center">
                                   <div class="menu-toggle">
                                       <span class="menu__bar"></span>
                                       <span class="menu__bar"></span>
                                       <span class="menu__bar"></span>
                                   </div><!-- end menu-toggle -->
                               </div>
                           </div><!-- end logo -->
                           <div class="main-menu-content ml-auto">
                               <nav class="main-menu">
                                   <ul>
                                       <li><a href="/">Bookings</span></a></li>
                                       <li><a href="/">Finance</span></a></li>
                                   </ul>
                               </nav>
                           </div><!-- end main-menu-content -->
                       </div><!-- end menu-full-width -->
                   </div><!-- end col-lg-12 -->
               </div><!-- end row -->
           </div><!-- end container-fluid -->
       </div><!-- end header-menu-wrapper -->
   </header>
   <!-- ================================
         END HEADER AREA
================================= -->
