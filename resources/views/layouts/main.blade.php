<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Knot-X - Accommodation & Service Listing</title>
    <!-- Favicon -->
    <link rel="icon" href="/assets/images/favicon.png">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <x-maincss />
    <style>
        * {
            font-family: 'Poppins', sans-serif !important;
        }

    </style>

    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@mdi/font@6.5.95/css/materialdesignicons.min.css">
</head>

<body>
    <x-header />

    <x-modal.signin />

    <x-modal.signup />

    <x-modal.forgetpassword />

    @yield('content')

    <x-footer />

    <!-- start back-to-top -->
    <div id="back-to-top">
        <i class="mdi mdi-arrow-up" title="Go top"></i>
    </div>
    <!-- end back-to-top -->
    <x-mainjs />
</body>

</html>
